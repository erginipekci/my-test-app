import { Component, OnInit } from '@angular/core';
import { ListService } from './list.service';
import { ProductList} from './shopping-list.mock';
import { List} from './list';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ListService]
})
export class ListComponent implements OnInit {

  products: List[];
  listname = 'Alışveriş Listesi';
  constructor() {
    this.products = ProductList;
  }

  ngOnInit() {
  }

}
