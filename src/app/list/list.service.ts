import { Injectable } from '@angular/core';
import { ProductList } from './shopping-list.mock';
import { List} from './list';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  products: List[];
  constructor() { }
}
