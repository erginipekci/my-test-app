import { List } from './list';

export const ProductList: List[] = [
    { productId: 1, productName: 'Macbook' },
    { productId: 2, productName: 'Lenova' }
];
